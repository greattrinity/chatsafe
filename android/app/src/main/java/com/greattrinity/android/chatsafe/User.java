package com.greattrinity.android.chatsafe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.security.Key;

public class User {
    @SerializedName("login")
//    @Expose
//    private byte[] login;
//    @SerializedName("password")
//    @Expose
//    private byte[] password;
    @Expose
    private  String login;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("public_key")
    @Expose
    private String p_key;

    @SerializedName("message")
    @Expose
    private String message;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    public String getPKey() {
        return p_key;
    }

    public void setPKey(String p_key) {
        this.p_key = p_key;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getMessage(){return message;}

}
