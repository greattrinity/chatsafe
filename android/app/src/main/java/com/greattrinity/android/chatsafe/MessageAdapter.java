package com.greattrinity.android.chatsafe;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter {

    private static final int TYPE_MESSAGE_SENT = 1;
    private static final int TYPE_MESSAGE_RECEIVED = 2;
    private static final int TYPE_IMAGE_SENT = 3;
    private static final int TYPE_IMAGE_RECEIVED = 4;

    private LayoutInflater inflater;
    private List<JSONObject> messages = new ArrayList<>();

    public MessageAdapter (LayoutInflater inflater) {
        this.inflater = inflater;
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {

        TextView mMessageText;

        public SentMessageHolder(@NonNull View itemView) {
            super(itemView);

            mMessageText = itemView.findViewById(R.id.sent_text);
        }
    }

    private class SentImageHolder extends RecyclerView.ViewHolder {

        ImageView mImage;

        public SentImageHolder(@NonNull View itemView) {
            super(itemView);

            mImage = itemView.findViewById(R.id.sent_image);
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {

        TextView mNameText, mMessageText;

        public ReceivedMessageHolder(@NonNull View itemView) {
            super(itemView);

            mNameText = itemView.findViewById(R.id.name_text);
            mMessageText = itemView.findViewById(R.id.received_text);
        }
    }

    private class ReceivedImageHolder extends RecyclerView.ViewHolder {

        TextView mNameText;
        ImageView mImage;

        public ReceivedImageHolder(@NonNull View itemView) {
            super(itemView);

            mNameText = itemView.findViewById(R.id.name_text);
            mImage = itemView.findViewById(R.id.received_image);
        }
    }

    @Override
    public int getItemViewType(int position) {
        JSONObject message = messages.get(position);

        try {
            if (message.getBoolean("isSent")) {
                if (message.has("message")) {
                    return TYPE_MESSAGE_SENT;
                } else {
                    return TYPE_IMAGE_SENT;
                }
            } else {
                if (message.has("message")) {
                    return TYPE_MESSAGE_RECEIVED;
                } else {
                    return TYPE_IMAGE_RECEIVED;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_MESSAGE_SENT:
                view = inflater.inflate(R.layout.item_sent_message, parent, false);
                return new SentMessageHolder(view);
            case TYPE_MESSAGE_RECEIVED:
                view = inflater.inflate(R.layout.item_received_message, parent, false);
                return new ReceivedMessageHolder(view);
            case TYPE_IMAGE_SENT:
                view = inflater.inflate(R.layout.item_sent_image, parent, false);
                return new SentImageHolder(view);
            case TYPE_IMAGE_RECEIVED:
                view = inflater.inflate(R.layout.item_received_image, parent, false);
                return new ReceivedImageHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        JSONObject message = messages.get(position);

        try {
            if (message.getBoolean("isSent")) {
                if (message.has("message")) {
                    SentMessageHolder messageHolder = (SentMessageHolder) holder;
                    messageHolder.mMessageText.setText(message.getString("message"));
                } else {
                    SentImageHolder imageHolder = (SentImageHolder) holder;
                    Bitmap image = getBitmapFromString(message.getString("image"));
                    imageHolder.mImage.setImageBitmap(image);
                }
            } else {
                if (message.has("message")) {
                    ReceivedMessageHolder messageHolder = (ReceivedMessageHolder) holder;
                    messageHolder.mNameText.setText(message.getString("name"));
                    messageHolder.mMessageText.setText(message.getString("message"));
                } else {
                    ReceivedImageHolder imageHolder = (ReceivedImageHolder) holder;
                    imageHolder.mNameText.setText(message.getString("name"));
                    Bitmap image = getBitmapFromString(message.getString("image"));
                    imageHolder.mImage.setImageBitmap(image);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Bitmap getBitmapFromString(String image) {
        byte[] bytes = Base64.decode(image, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes,0, bytes.length);
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void addItem (JSONObject jsonObject) {
        messages.add(jsonObject);
        notifyDataSetChanged();
    }
}
