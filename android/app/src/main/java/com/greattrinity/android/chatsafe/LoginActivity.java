package com.greattrinity.android.chatsafe;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        EditText login = (EditText) findViewById(R.id.email_et);
        EditText password = (EditText) findViewById(R.id.password_et);
        Button btn = (Button) findViewById(R.id.login_btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                    getPublicKey(login, password);

            }
        });

    }

    private void getPublicKey(EditText login, EditText password) {
        RetroClient.getInstance()
                .getApiService()
                .getPublicKey()
                .enqueue(new Callback<User>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }

                        String pubKey = response.body().getPKey();

                        RSA rsa = new RSA();
                        try {
                            String login_enc = rsa.encrypt(pubKey, login.getText().toString());
                            String pass_enc = rsa.encrypt(pubKey, password.getText().toString());

                            PPKeys keys = rsa.createKeys(1024);
                            loginUser(login_enc, pass_enc, keys.getPublicKey(), keys);
                        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void loginUser(String login, String password, String pubKey, PPKeys keys) {
        RetroClient.getInstance()
                .getApiService()
                .loginUser(login, password, pubKey)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }

                        RSA rsa = new RSA();
                        try {
                            String res = rsa.decrypt(keys.getPrivatekey(), response.body().getMessage());
                            System.out.println(res);
                            if(res.equals("user exists")) {
                                Intent intent = new Intent(LoginActivity.this, Menu.class);
                                startActivity(intent);
                            }
                        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }
}
