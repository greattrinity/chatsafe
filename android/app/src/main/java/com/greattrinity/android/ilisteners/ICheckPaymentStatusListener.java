package com.greattrinity.android.listeners;

import com.greattrinity.android.service.background.params.CheckPaymentStatusParams;

public interface ICheckPaymentStatusListener {
    void CheckPaymentStatusResult(CheckPaymentStatusParams params);
}