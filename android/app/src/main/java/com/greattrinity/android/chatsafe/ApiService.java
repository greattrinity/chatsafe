package com.greattrinity.android.chatsafe;
import java.security.Key;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {
    @FormUrlEncoded
    @POST("/post")
    Call<User> loginUser(
            @Field("login") String login,
            @Field("password") String password,
            @Field("public_key") String public_key
    );

    @GET("/publickey")
    Call<User> getPublicKey();
}