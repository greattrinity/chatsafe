package com.greattrinity.android.listeners;

import com.greattrinity.android.service.background.params.CheckoutPaymentParams;

public interface ICheckoutResultListener {
    void checkoutResult(CheckoutPaymentParams params);
}
