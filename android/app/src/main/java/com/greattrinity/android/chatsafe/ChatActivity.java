package com.greattrinity.android.chatsafe;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class ChatActivity extends AppCompatActivity implements TextWatcher {

    private String mName;
    private WebSocket mWebSocket;
    private EditText mMessageEdit;
    private View mSendButton, mPickImageButton;
    private RecyclerView mRecyclerView;
    private MessageAdapter mMessageAdapter;

    private static final String SERVER_PATH = "ws://192.168.31.24:3000";

    private static final int IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mName = getIntent().getStringExtra("name");
        initiateSocketConnection();
    }

    private void initiateSocketConnection() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(SERVER_PATH).build();
        mWebSocket = client.newWebSocket(request, new SocketListener());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String string = s.toString().trim();
        if(string.isEmpty()) {
            resetMessageEdit();
        } else {
            mSendButton.setVisibility(View.VISIBLE);
            mPickImageButton.setVisibility(View.INVISIBLE);
        }
    }

    private void resetMessageEdit() {
        mMessageEdit.removeTextChangedListener(this);

        mMessageEdit.setText("");
        mSendButton.setVisibility(View.INVISIBLE);
        mPickImageButton.setVisibility(View.VISIBLE);

        mMessageEdit.addTextChangedListener(this);
    }

    private class SocketListener extends WebSocketListener {
        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);

            runOnUiThread(() -> {
                Toast.makeText(ChatActivity.this, R.string.connection_successful,
                        Toast.LENGTH_LONG);

                initializeView();
            });
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            super.onMessage(webSocket, text);

            runOnUiThread(() -> {
                try {
                    JSONObject jsonObject = new JSONObject(text);
                    jsonObject.put("isSent", false);

                    mMessageAdapter.addItem(jsonObject);

                    mRecyclerView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private void initializeView() {
        mMessageEdit = findViewById(R.id.message_edit);
        mPickImageButton = findViewById(R.id.pick_image_button);
        mSendButton = findViewById(R.id.send_button);

        mRecyclerView = findViewById(R.id.recycler_view);
        mMessageAdapter = new MessageAdapter(getLayoutInflater());
        mRecyclerView.setAdapter(mMessageAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mMessageEdit.addTextChangedListener(this);

        mSendButton.setOnClickListener(v -> {
            JSONObject jsonObject = new JSONObject();

            try{
                jsonObject.put("name", mName);
                jsonObject.put("message", mMessageEdit.getText().toString());

                mWebSocket.send(jsonObject.toString());

                jsonObject.put("isSent", true);

                mMessageAdapter.addItem(jsonObject);

                mRecyclerView.scrollToPosition(mMessageAdapter.getItemCount() - 1);

                resetMessageEdit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        mPickImageButton.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");

            startActivityForResult(Intent.createChooser(intent, getString(R.string.pick_image)),
                    IMAGE_REQUEST);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == IMAGE_REQUEST) {
            try {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                Bitmap image = BitmapFactory.decodeStream(inputStream);
                sendImage(image);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendImage(Bitmap image) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);

        String base64String = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", mName);
            jsonObject.put("image", base64String);

            mWebSocket.send(jsonObject.toString());

            jsonObject.put("isSent", true);

            mMessageAdapter.addItem(jsonObject);

            mRecyclerView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
