const SocketServer = require('websocket').server
const express = require("express");
const http = require('http')
const WebSocket = require('ws');
const app = express();
const jsonParser = express.json();
const bodyParser = require("body-parser");
const crypto = require('crypto');
const { generateKeyPairSync } = require('crypto')
//RSA
const NodeRSA = require('node-rsa');

const key = NodeRSA({b: 1024});
const private_key = key.exportKey("pkcs8-private");
const public_key = key.exportKey("pkcs8-public-pem");

function DecryptCredentials(cred, key){
  var decCred = key.decrypt(cred, 'utf8');
  console.log(decCred);
}

function EncriptCredentials(cred, key){
  var encrCred = key.encrypt(cred, 'base64');
  return encrCred;
}

const server = http.createServer(app);

let allowCrossDomain = function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Headers', "*");
  next();
}
app.use(allowCrossDomain);

app.use(bodyParser.urlencoded({ extended: false }));

//отправляем публичный ключ
app.get("/publickey", function(req, res){
      res.status(200).json({public_key: public_key});
});

//принимаем креды
app.post("/post", jsonParser, function(req, res){
  if(!req.body) return res.sendStatus(400);
    const login = req.body.login;

    const pass = req.body.password;

    const clientpKey = req.body.public_key
    console.log(clientpKey);
    const cpkey = new NodeRSA(clientpKey);
    console.log("Расшифрованный логин: ");
    DecryptCredentials(login, key);
    console.log("Расшифрованный пароль: ");
    DecryptCredentials(pass, key);


    res.status(200).json({login: login, password: pass, message: EncriptCredentials("user exists", cpkey)});
});
server.listen(3000, () => {
    console.log("Listening on port 3000...")
})


wsServer = new WebSocket.Server({ server });
const connections = []

wsServer.on('request', (req) => {
    const connection = req.accept()
    console.log('new connection')
    connections.push(connection)

    connection.on('message', (mes) => {
        connections.forEach(element => {
            if (element != connection)
                element.sendUTF(mes.utf8Data)
        })
    })

    connection.on('close', (resCode, des) => {
        console.log('connection closed')
        connections.splice(connections.indexOf(connection), 1)
    })
})
